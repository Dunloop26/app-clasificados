import { useState, useEffect } from "react";

import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";

import "./FormView.css";

const formList = [LoginForm, RegisterForm];

export default function FormView({ index = 0 }) {
  const [tabs, setTabs] = useState(["Iniciar Sesión", "Registro"]);
  const [tabIndex, setTabIndex] = useState(index);
  const [form, setForm] = useState(LoginForm);

  useEffect(() => {
    setForm(formList[tabIndex]);
  }, [tabIndex]);

  function tabClick(idx) {
    setTabIndex(idx);
  }

  return (
    <div className="form-view-container">
      <main className="form-view">
        <section className="tab">
          {tabs.map((tabItem, index) => {
            const tabClass = index === tabIndex ? "active" : "";
            return (
              <h4 className={tabClass} href="#" key={index} onClick={() => tabClick(index)}>
              {tabItem}
              </h4>
            );
          })}
        </section>
        <section className="form-section">
          {form}
        </section>
      </main>
    </div>
  );
}
