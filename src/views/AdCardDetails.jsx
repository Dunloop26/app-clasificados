import { useContext, useEffect, useState } from "react";
import GlobalContext from "../contexts/GlobalContext";

import "./AdCardDetails.css";
import placeholderImg from "../assets/house-placeholder.jpg";
import { BiCalendarEvent, BiLoaderAlt } from "react-icons/bi";

export default function AdCardDetails({ id }) {
  const contextData = useContext(GlobalContext);
  const [cards, setCards] = contextData.cardState;

  const [card, setCard] = useState({});
  const [loading, setLoading] = useState(true);
  const [placeholder, setPlaceholder] = useState(false);
  const [image, setImage] = useState("");

  useEffect(() => {
    contextData.updateCards();
    //setTimeout(() => {
    //setLoading(false);
    //}, 5000);
  }, [contextData]);

  useEffect(() => {
    if (card && "img" in card) {
      setImage(card.img);
    }
  }, [card]);

  useEffect(() => {
    function selectCard(id) {
      return cards.find((card) => {
        return card.id === id;
      });
    }

    setCard(selectCard(id));
  }, [cards, id]);

  function onImageLoad() {
    console.log("Load ended");
    setLoading(false);
  }

  function onImageError(e) {
    console.error("Image error:", e);
    setLoading(false);
    setPlaceholder(true);
    setImage(placeholderImg);
  }

  return (
    <main className="ad-card-details">
      {card ? (
        <section className="ad-container">
          <section className="photo-view">
            <BiLoaderAlt className={`loader ${loading ? "" : "hidden"}`} />
            <img
              className={loading ? "hidden" : ""}
              src={image}
              onLoad={onImageLoad}
              onError={(e) => {
                onImageError(e);
              }}
              alt="House"
            />
          </section>
          <section className="ad-info">
            <h3 className="title">{card.title}</h3>
            <h3 className="contact">{card.contacto}</h3>
            {card.descripcion ? (
              <p className="description">{card.descripcion}</p>
            ) : (
              <p className="description">Sin descripción</p>
            )}
            <div className="icon-row">
              <BiCalendarEvent />
              <p className="start date">{card.fechaInicial}</p>
            </div>
            <div className="icon-row">
              <BiCalendarEvent />
              <p className="end date">{card.fechaFinal}</p>
            </div>
          </section>
        </section>
      ) : (
        <h1>Not found</h1>
      )}
    </main>
  );
}
