export default function RegisterForm() {
  return <form>
      <div className="input-group">
        <label htmlFor="register-nombres">Nombres</label>
        <input id="register-nombres" type="text" name="nombres" />
      </div>
      <div className="input-group">
        <label htmlFor="register-apellidos">Apellidos</label>
        <input id="register-apellidos" type="text" name="apellidos" />
      </div>
      <div className="input-group">
        <label htmlFor="register-correo">Correo</label>
        <input id="register-correo" type="email" name="correo" />
      </div>
      <div className="input-group">
        <label htmlFor="register-telefono">Teléfono</label>
        <input id="register-telefono" type="email" name="telefono" />
      </div>
      <div className="input-group">
        <label htmlFor="register-password">Contraseña</label>
        <input id="register-password" type="password" name="password" />
      </div>
      <div className="terminos">
        <input type="checkbox" name="terminos" id="terminos" />
        <label htmlFor="terminos">
          Acepto todos los{" "}
          <span className="highlight">Términos y Condiciones</span> de las{" "}
          <span className="highlight">Políticas de Privacidad</span>
        </label>
      </div>
    <button type="button">Registrarse</button>
    </form>;
}
