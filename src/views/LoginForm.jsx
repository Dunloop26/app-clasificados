export default function Login() {
  return (
    <form>
      <div className="input-group">
        <label htmlFor="login-email">Email</label>
        <input id="login-email" type="email" name="email" />
      </div>
      <div className="input-group">
        <label htmlFor="login-pass">Contraseña</label>
        <input id="login-pass" type="password" name="password" />
      </div>
      <div className="terminos">
        <input type="checkbox" name="terminos" id="terminos" />
        <label htmlFor="terminos">
          Acepto todos los{" "}
          <span className="highlight">Términos y Condiciones</span> de las{" "}
          <span className="highlight">Políticas de Privacidad</span>
        </label>
      </div>

      <button type="button">Iniciar Sesión</button>
    </form>
  );
}
