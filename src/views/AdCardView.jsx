import { useEffect, useState, useContext } from "react";
import AdCard from "../components/AdCard";
import GlobalContext from "../contexts/GlobalContext";

import "./AdCardView.css";

export default function AdCardView() {

  const contextData = useContext(GlobalContext);
  const [cards, setCards] = contextData.cardState;

  useEffect(() => {
    contextData.updateCards()
  }, []);

  return (
    <main className="main-view">
      {
        cards.map((data, index) => {
        return <AdCard key={index} {...data} />
      })
      }
    </main>
  );
}
