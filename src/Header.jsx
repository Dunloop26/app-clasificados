import { Link } from "wouter";

import "./Header.css";

function Header() {
  return (
    <header className="app-header">
      <div className="app-image">
        <img src="" alt="Logo" />
        <Link to="/">
          <h1>App Clasificados</h1>
        </Link>
      </div>
      <nav>
        <ul>
          <li>
            <Link to="/form">Iniciar Sesión</Link>
          </li>
          <li>
            <Link to="/form">Regístrate</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
