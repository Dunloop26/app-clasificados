// Wouter
import { useLocation } from "wouter";
// React
import { useContext } from "react";

// Icons
import { BiBed, BiMoney, BiUserCircle, BiSpreadsheet } from "react-icons/bi";

import GlobalContext from "../contexts/GlobalContext";

import placeholder from "../assets/house-placeholder.jpg";

import "./AdCard.css";

export default function AdCard({
  id,
  img = null,
  cantidadHabitaciones = 0,
  precio = 0,
  contacto = "",
  descripcion = "",
}) {
  img = img ?? placeholder;

  const [, setLocation] = useLocation();
  const [, setCardId] = useContext(GlobalContext).idState;

  function truncateText(text, charCount) {
    if (text.length >= charCount) return text.substring(0, charCount) + "...";
    else return text;
  }

  function onLinkClick(link, id) {
    setCardId(id);
    setLocation(`${link}/${id}`);
  }

  return (
    <button
      className="button-container"
      onClick={() => onLinkClick("/clasificado", id)}
    >
      <article className="ad-card-item">
        <header>
          <img className="ad-card-img" src={img} alt="House" />
        </header>
        <section className="info">
          <BiUserCircle />
          <p>Contacto:</p>
          <span className="contact">{contacto}</span>
          <BiBed />
          <p>Habitaciones:</p>
          <span className="rooms">{cantidadHabitaciones}</span>
          <BiMoney />
          <p>Precio:</p>
          <span className="price">{precio}</span>
          <BiSpreadsheet />
          <p>Descripcion:</p>
          {descripcion ? (
            <span className="description">
              {truncateText(descripcion, 100)}
            </span>
          ) : (
            <span className="description">
              Sin descripción
            </span>
          )}
        </section>
      </article>
    </button>
  );
}
