// React
import { useState } from "react";
// Wouter
import { Route } from "wouter";

// Componentes
import Header from "./Header";
import AdCardView from "./views/AdCardView";
import FormView from "./views/FormView";
import AdCardDetails from "./views/AdCardDetails";
import GlobalContext from "./contexts/GlobalContext"

import "./App.css";
import data from "./mock/data.json"

export default function App() {

  const cardState = useState([])
  const idState = useState("")

  const contextData = {
    cardState,
    idState,
    updateCards: () => {
      const [, setCards] = cardState;
      setCards(data)
    }
  }

  return (
    <main className="app-container">
      <Header />
      <section className="app-view">
        <GlobalContext.Provider value={contextData}>
          <Route path="/">
            <AdCardView />
          </Route>
          <Route path="/clasificado/:id">
            {({ id }) => <AdCardDetails id={id} />}
          </Route>
        </GlobalContext.Provider>
        <Route path="/form">
          <FormView />
        </Route>
      </section>
    </main>
  );
}
